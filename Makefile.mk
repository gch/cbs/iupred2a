libiupred2a := libiupred2a$(DYN_LIB_EXT)

BINS-y := $(libiupred2a)

objs := \
	src/iupred2a.c.o \
	src/loader.c.o \
	util/file.c.o \
	util/util.c.o

OBJS-$(libiupred2a)-y := $(objs)

CFLAGS-y += \
	-I$(MKS_PROJDIR)

LDFLAGS-$(libiupred2a)-y += \
	$(LDFLAGS_SHARED) \
	-lm
