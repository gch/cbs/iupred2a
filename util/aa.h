#ifndef UTIL_AA_H
#define UTIL_AA_H 1

enum u_aa_type
{
	U_AA_INVALID = 0,
	U_AA_A,
	U_AA_UNUSED2,
	U_AA_C,
	U_AA_D,
	U_AA_E,
	U_AA_F,
	U_AA_G,
	U_AA_H,
	U_AA_I,
	U_AA_UNUSED10,
	U_AA_K,
	U_AA_L,
	U_AA_M,
	U_AA_N,
	U_AA_UNUSED15,
	U_AA_P,
	U_AA_Q,
	U_AA_R,
	U_AA_S,
	U_AA_T,
	U_AA_UNUSED21,
	U_AA_V,
	U_AA_W,
	U_AA_UNUSED24,
	U_AA_Y,
	U_AA_UNUSED26,
	U_AA_COUNT
};

static inline enum u_aa_type u_aa_type_from_char(int aa) {
	if (aa >= 'A' && aa <= 'Z')
		return U_AA_A + aa - 'A';

	return U_AA_INVALID;
}

static inline int u_aa_type_to_char(enum u_aa_type type) {
	if (type <= U_AA_INVALID || type >= U_AA_COUNT)
		return '?';

	return 'A' + type - U_AA_A;
}

#endif /* UTIL_AA_H */
