#ifndef UTIL_UTIL_H
#define UTIL_UTIL_H 1

#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"

#define U_ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

#define u_ofoffset(type, st, off) (*(type *)&((char *)(void *)st)[off])

#define U_ID_ALNUM64_LEN 12
#define U_ID_ALNUM64_SIZE (U_ID_ALNUM64_LEN + 1)

#define U_ID_NUM64_LEN 20
#define U_ID_NUM64_SIZE (U_ID_NUM64_LEN + 1)

#define U_ASCII_CHAR_COUNT 128
#define U_ASCII_CHAR_MASK 0x7F

static inline float u_min2f(float a, float b) {
	return a < b ? a : b;
}

static inline int32_t u_min2i32(int32_t a, int32_t b) {
	return a < b ? a : b;
}

static inline long u_min2l(long a, long b) {
	return a < b ? a : b;
}

static inline size_t u_min2sz(size_t a, size_t b) {
	return a < b ? a : b;
}

static inline float u_max2f(float a, float b) {
	return a > b ? a : b;
}

static inline int32_t u_max2i32(int32_t a, int32_t b) {
	return a > b ? a : b;
}

static inline long u_max2l(long a, long b) {
	return a > b ? a : b;
}

static inline size_t u_max2sz(size_t a, size_t b) {
	return a > b ? a : b;
}

static inline float u_clampf(float l, float a, float h) {
	return u_min2f(u_max2f(l, a), h);
}

static inline long u_clampl(long l, long a, long h) {
	return u_min2l(u_max2l(l, a), h);
}

static inline size_t u_clampsz(size_t l, size_t a, size_t h) {
	return u_min2sz(u_max2sz(l, a), h);
}

static inline int32_t u_clampi32(int32_t l, int32_t a, int32_t h) {
	return u_min2i32(u_max2i32(l, a), h);
}

static inline void u_free_p(void *pptr) {
	void **p = pptr;
	if (!*p)
		return;

	free(*p);
	*p = NULL;
}

#if ULONG_MAX >= UINT32_MAX
static inline uint32_t u_strtou32(const char *nptr, char **endptr, int base) {
	unsigned long n = strtoul(nptr, endptr, base);

	if (n > UINT32_MAX)
	{
		n = UINT32_MAX;
		errno = ERANGE;
	}

	return (uint32_t)n;
}

#elif ULLONG_MAX >= UINT32_MAX

static inline uint32_t u_strtou32(const char *nptr, char **endptr, int base) {
	unsigned long long n = strtoull(nptr, endptr, base);

	if (n > UINT32_MAX)
	{
		n = UINT32_MAX;
		errno = ERANGE;
	}

	return (uint32_t)n;
}

#else
#  error Could not map any long type to uint32_t
#endif

#if ULONG_MAX >= UINT64_MAX

static inline uint64_t u_strtou64(const char *nptr, char **endptr, int base) {
	unsigned long n = strtoul(nptr, endptr, base);

	if (n > UINT64_MAX)
	{
		n = UINT64_MAX;
		errno = ERANGE;
	}

	return (uint64_t)n;
}

#elif ULLONG_MAX >= UINT64_MAX

static inline uint64_t u_strtou64(const char *nptr, char **endptr, int base) {
	unsigned long long n = strtoull(nptr, endptr, base);

	if (n > UINT64_MAX)
	{
		n = UINT64_MAX;
		errno = ERANGE;
	}

	return (uint64_t)n;
}

#else
#  error Could not map any long type to uint64_t
#endif

extern uint64_t u_id_num64_to_u64(const char *id);
extern char *u_id_u64_to_num64(char buf[U_ID_NUM64_SIZE], uint64_t id);
extern uint64_t u_id_alnum64_to_u64(const char *id);
extern char *u_id_u64_to_alnum64(char buf[U_ID_ALNUM64_SIZE], uint64_t id);
extern char *u_str_concat(const char *s1, ...);
extern char *u_strdup(const char *str);
extern char *u_strtok_r(char *str, const char *delim, char **saveptr);

#endif /* UTIL_UTIL_H */
