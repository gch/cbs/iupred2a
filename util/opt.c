/*
 * Obfu9
 * Copyright (C) 2020 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "opt.h"


static int parse_opts_helper(int argc, char **argv, struct u_opt *opts)
{
	int result;
	size_t i;
	size_t arg_count = (size_t)argc;

	for (struct u_opt *opt = opts; opt->type != U_OPT_TYPE_INVAL; opt++)
	{
		opt->defined = 0;
		opt->data.as_str = NULL;
	}

	for (i = 0; i < arg_count; i++)
	{
		char *arg = argv[i];
		int matched = 0;

		for (struct u_opt *opt = opts; opt->type != U_OPT_TYPE_INVAL; opt++)
		{
			result = strcmp(arg, opt->name);
			if (result != 0)
				continue;

			matched = 1;
			/* Reset a redefined opt */
			opt->defined = 0;

			if (opt->type != U_OPT_TYPE_NONE)
			{
				if (i >= arg_count - 1)
					return -1;

				opt->data.as_str = argv[++i];
			}
			else
				opt->data.as_str = "";
		}

		if (!matched)
			return -1;
	}

	arg_count = i;

	for (struct u_opt *opt = opts; opt->type != U_OPT_TYPE_INVAL; opt++)
	{
		if (!opt->data.as_str)
			continue;

		switch (opt->type)
		{
			case U_OPT_TYPE_INVAL:
			case U_OPT_TYPE_NONE:
			case U_OPT_TYPE_STR:
				break;

			case U_OPT_TYPE_LONG:
			{
				const char *p = opt->data.as_str;
				char *endp;
				long n;

				if (!*p)
					return -1;

				n = strtol(p, &endp, 0);
				if (*endp)
					return -1;

				opt->data.as_long = n;
				break;
			}
		}

		opt->defined = 1;
	}

	return arg_count;
}

struct u_opt *u_opt_parse(int argc, char **argv, struct u_opt *tpl)
{
	int count;

	if (argc < 1 || !argv)
		return NULL;

	argc--;
	argv++;

	count = parse_opts_helper(argc, argv, tpl);
	if (count < 0)
		return NULL;

	return tpl;
}

int u_opt_get_defined(const struct u_opt *opts, size_t id)
{
	return opts[id].defined;
}

long u_opt_get_long(const struct u_opt *opts, size_t id)
{
	if (!opts[id].defined || opts[id].type != U_OPT_TYPE_LONG)
		return LONG_MAX;

	return opts[id].data.as_long;
}

long u_opt_get_long_or(const struct u_opt *opts, size_t id, long default_val)
{
	long val = u_opt_get_long(opts, id);

	return val != LONG_MAX ? val : default_val;
}

const char *u_opt_get_str(const struct u_opt *opts, size_t id)
{
	if (!opts[id].defined || opts[id].type != U_OPT_TYPE_STR)
		return NULL;

	return opts[id].data.as_str;
}

const char *u_opt_get_str_or(const struct u_opt *opts, size_t id, const char *default_val)
{
	const char *val = u_opt_get_str(opts, id);

	return val ? val : default_val;
}
