/*
 * Copyright (C) 2017-2019 Guillaume Charifi
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "file.h"

#define BLK_SIZE 4096


static int std_mode_from_file_mode(char std_mode[4], enum u_file_open_mode mode)
{
	if (!(mode & U_FILE_OPEN_READ)
			&& !(mode & U_FILE_OPEN_WRITE))
		return -1;

	if (mode & U_FILE_OPEN_READ)
		*std_mode++ = 'r';

	if (mode & U_FILE_OPEN_WRITE)
		*std_mode++ = 'w';

	if (mode & U_FILE_OPEN_BIN)
		*std_mode++ = 'b';

	*std_mode++ = '\0';
	return 0;
}

int u_file_open(struct u_file *file, const char *path, enum u_file_open_mode mode)
{
	int status;
	char std_mode[4];
	FILE *fp;

	file->fp = NULL;
	file->mode = 0;
	file->size = 0;

	status = std_mode_from_file_mode(std_mode, mode);
	if (status < 0)
		return status;

	fp = fopen(path, std_mode);
	if (!fp)
		return -1;

	return u_file_import(file, fp, mode);
}

int u_file_import(struct u_file *file, FILE *fp, enum u_file_open_mode mode)
{
	file->fp = fp;
	file->mode = mode;
	file->size = 0;

	return 0;
}

size_t u_file_get_size(struct u_file *file)
{
	FILE *fp = file->fp;
	long old, size;

	old = ftell(fp);
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fseek(fp, old, SEEK_SET);

	if (size < 0)
		return file->size;

	return size;
}

void *u_file_get_contents(struct u_file *file, size_t *read_len)
{
	int status;
	FILE *fp = file->fp;
	size_t size;
	char *data = NULL;

	size = u_file_get_size(file);

	if (size == U_FILE_SIZE_INVAL)
	{
		size = 0;

		while (!feof(fp))
		{
			char *new_data;
			uint64_t read_size = 0;

			new_data = realloc(data, size + BLK_SIZE + 1);
			if (!new_data)
				goto free_exit;
			data = new_data;

			read_size = fread(&data[size], 1, BLK_SIZE, fp);
			if (ferror(fp))
				goto free_exit;

			size += read_size;
		}
	}
	else if (size > 0)
	{
		data = malloc(size + 1);
		if (!data)
			goto free_exit;

		status = fread(data, size, 1, fp);
		if (status != 1)
			goto free_exit;
	}

	data[size] = '\0';

	if (read_len)
		*read_len = size;

	return data;

free_exit:
	if (data)
		free(data);

	if (read_len)
		*read_len = U_FILE_SIZE_INVAL;

	return NULL;
}

int u_file_close(struct u_file *file)
{
	if (file->fp)
		fclose(file->fp);
	file->fp = NULL;

	return 0;
}
