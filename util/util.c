#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

uint64_t u_id_num64_to_u64(const char *id)
{
	return u_strtou64(id, NULL, 10);
}

char *u_id_u64_to_num64(char buf[U_ID_NUM64_SIZE], uint64_t id)
{
	buf = &buf[U_ID_NUM64_SIZE];
	*--buf = '\0';

	do
	{
		char c = id % 10;
		id /= 10;

		*--buf = c + '0';
	}
	while (id > 0);

	return buf;
}

uint64_t u_id_alnum64_to_u64(const char *id)
{
	return u_strtou64(id, NULL, 36);
}

char *u_id_u64_to_alnum64(char buf[U_ID_ALNUM64_SIZE], uint64_t id)
{
	buf = &buf[U_ID_ALNUM64_SIZE];
	*--buf = '\0';

	do
	{
		char c = id % 36;
		id /= 36;

		if (c <= 9)
			c += '0';
		else
			c += 'A' - 10;

		*--buf = c;
	}
	while (id > 0);

	return buf;
}

char *u_str_concat(const char *s1, ...)
{
	va_list ap;
	const char *s;
	size_t size = 0;
	char *dst;
	char *d;

	va_start(ap, s1);

	s = s1;

	while (s)
	{
		size += strlen(s);

		s = va_arg(ap, const char *);
	}

	va_end(ap);

	dst = malloc(size + 1);
	if (!dst)
		return NULL;

	d = dst;

	va_start(ap, s1);

	s = s1;

	while (s)
	{
		size_t len;

		len = strlen(s);
		memcpy(d, s, len);
		d += len;

		s = va_arg(ap, const char *);
	}

	va_end(ap);

	*d = '\0';

	return dst;
}

char *u_strdup(const char *str)
{
	size_t size = strlen(str) + 1;
	char *s = malloc(size);
	if (!s)
		return NULL;

	memcpy(s, str, size);
	return s;
}

char *u_strtok_r(char *str, const char *delim, char **saveptr)
{
	size_t len;
	char *r;

	if (!str)
		str = *saveptr;

	/* Skip delimiters */
	len = strspn(str, delim);
	str += len;

	if (!*str)
		return NULL;

	/* Extract until delimiter */
	len = strcspn(str, delim);
	r = str;

	str += len;
	if (*str)
		*str++ = '\0';

	*saveptr = str;
	return r;
}
