/*
 * Obfu9
 * Copyright (C) 2020 Guillaume Charifi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTIL_OPT_H
#define UTIL_OPT_H 1

#include <stddef.h>

#define U_OPT_NONE(e, n) [e] = { .type = U_OPT_TYPE_NONE, .name = n }
#define U_OPT_STR(e, n) [e] = { .type = U_OPT_TYPE_STR, .name = n }
#define U_OPT_LONG(e, n) [e] = { .type = U_OPT_TYPE_LONG, .name = n }
#define U_OPT_END(e) [e] = { .type = U_OPT_TYPE_INVAL, .name = NULL }

enum u_opt_type
{
	U_OPT_TYPE_INVAL = 0,
	U_OPT_TYPE_NONE,
	U_OPT_TYPE_LONG,
	U_OPT_TYPE_STR
};

struct u_opt
{
	enum u_opt_type type;
	const char *name;
	unsigned int defined : 1;
	union {
		long as_long;
		const char *as_str;
	} data;
};


#ifdef __cplusplus
extern "C" {
#endif

extern struct u_opt *u_opt_parse(int argc, char **argv, struct u_opt *tpl);
extern int u_opt_get_defined(const struct u_opt *opts, size_t id);
extern long u_opt_get_long(const struct u_opt *opts, size_t id);
extern long u_opt_get_long_or(const struct u_opt *opts, size_t id, long default_val);
extern const char *u_opt_get_str(const struct u_opt *opts, size_t id);
extern const char *u_opt_get_str_or(const struct u_opt *opts, size_t id, const char *default_val);

#ifdef __cplusplus
}
#endif

#endif /* UTIL_OPT_H */
