#ifndef IUPRED_LOADER_H
#define IUPRED_LOADER_H 1

#include "util/aa.h"
#include "util/types.h"
#include "util/util.h"

#define IUPRED_DB CONFIG_IUPRED_DB

enum iupred_mode_type
{
	IUPRED_MODE_SHORT = 0,
	IUPRED_MODE_LONG,
	IUPRED_MODE_GLOB,
	IUPRED_MODE_COUNT
};

struct iupred_energy_matrix
{
	float e[U_AA_COUNT][U_AA_COUNT];
};

struct iupred_histogram
{
	size_t val_count;
	float *vals;
	float min;
	float max;
	float step;
	float lbound;
	float hbound;
};

struct iupred_mode_params
{
	unsigned long lc;
	unsigned long uc;
	unsigned long wc;
	float unweighted_score_adj;
};

struct iupred_mode
{
	enum iupred_mode_type type;
	struct iupred_mode_params params;
	struct iupred_energy_matrix matrix;
	struct iupred_histogram histo;
};

struct iupred_anchor2_params
{
	unsigned long local_window_size;
	unsigned long iupred_window_size;
	unsigned long local_smoothing_window;
	float a;
	float b;
	float c;
};

struct iupred_anchor2_iface_comp
{
	float freq[U_AA_COUNT];
};

struct iupred_anchor2
{
	struct iupred_anchor2_params params;
	struct iupred_energy_matrix matrix;
	struct iupred_anchor2_iface_comp iface_comp;
};

struct iupred_loader_context
{
	const char *path;
	struct iupred_mode mode[IUPRED_MODE_COUNT];
	struct iupred_anchor2 anchor2;
};

extern int iupred_loader_init(struct iupred_loader_context *ctx, const char *dir_name);
extern int iupred_loader_deinit(struct iupred_loader_context *ctx);
extern int iupred_loader_process(struct iupred_loader_context *ctx);

#endif /* IUPRED_LOADER_H */
