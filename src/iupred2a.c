#include <math.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "util/aa.h"
#include "util/types.h"
#include "util/util.h"

#include "iupred2a.h"

struct aa_freq2_context
{
	const struct iupred2a_aa *seq;
	size_t seq_len;
	struct
	{
		/* [l, h[ */
		long l;
		long h;
	} idx[2];
	size_t aa_count[U_AA_COUNT];
	size_t total;
};

static void iupred2a_aa_freq2_init(struct aa_freq2_context *ctx, size_t seq_len, const struct iupred2a_aa *seq) {
	ctx->seq = seq;
	ctx->seq_len = seq_len;
}

static void iupred2a_aa_freq2_set(struct aa_freq2_context *ctx, long idx0, size_t len0, long idx1, size_t len1)
{
	ctx->idx[0].l = idx0;
	ctx->idx[0].h = idx0 + len0;
	ctx->idx[1].l = idx1;
	ctx->idx[1].h = idx1 + len1;

	memset(ctx->aa_count, 0, sizeof(ctx->aa_count));
	ctx->total = 0;

	for (size_t i = 0; i < U_ARRAY_SIZE(ctx->idx); i++)
	{
		const size_t l = u_clampl(0, ctx->idx[i].l, ctx->seq_len);
		const size_t h = u_clampl(0, ctx->idx[i].h, ctx->seq_len);

		for (size_t j = l; j < h; j++)
			ctx->aa_count[ctx->seq[j].aa]++;

		ctx->total += h - l;
	}
}

static void iupred2a_aa_freq2_rshift1(struct aa_freq2_context *ctx)
{
	for (size_t i = 0; i < U_ARRAY_SIZE(ctx->idx); i++)
	{
		const long idxl = ctx->idx[i].l;
		const long idxh = ctx->idx[i].h;

		if (idxh >= 0 && idxh < (long)ctx->seq_len)
		{
			ctx->aa_count[ctx->seq[idxh].aa]++;

			if (idxl < 0)
				ctx->total++;
		}

		if (idxl >= 0 && idxl < (long)ctx->seq_len)
		{
			ctx->aa_count[ctx->seq[idxl].aa]--;

			if (idxh >= (long)ctx->seq_len)
				ctx->total--;
		}

		ctx->idx[i].l++;
		ctx->idx[i].h++;
	}
}

static inline float iupred2a_sum_inline(struct iupred2a_context *ctx, size_t off_prop, size_t low, size_t high)
{
	const struct iupred2a_aa *seq = ctx->seq;
	float score = 0.f;

	for (size_t i = low; i < high; i++)
	{
		const struct iupred2a_aa *aa = &seq[i];

		score += u_ofoffset(float, aa, off_prop);
	}

	return score;
}

static inline float iupred2a_mean_inline(struct iupred2a_context *ctx, size_t off_prop, size_t low, size_t high)
{
	const size_t count = high - low;

	return iupred2a_sum_inline(ctx, off_prop, low, high) / count;
}

static inline int32_t iupred2a_clamp(struct iupred2a_context *ctx, int32_t v)
{
	return u_clampi32(0, v, ctx->seq_len);
}

float iupred2a_sum(struct iupred2a_context *ctx, size_t off_prop, int32_t low, int32_t high)
{
	low = iupred2a_clamp(ctx, low);
	high = iupred2a_clamp(ctx, high);

	if (low >= high)
		return 0.f;

	return iupred2a_sum_inline(ctx, off_prop, low, high);
}

float iupred2a_mean(struct iupred2a_context *ctx, size_t off_prop, int32_t low, int32_t high)
{
	low = iupred2a_clamp(ctx, low);
	high = iupred2a_clamp(ctx, high);

	if (low >= high)
		return 0.f;

	return iupred2a_mean_inline(ctx, off_prop, low, high);
}

static void iupred2a_smooth(struct iupred2a_context *ctx, size_t off_src_aa, size_t off_dst_aa, size_t window)
{
	const size_t seq_len = ctx->seq_len;
	struct iupred2a_aa *seq = ctx->seq;

	for (size_t i = 0; i < ctx->seq_len; i++)
	{
		struct iupred2a_aa *aa = &seq[i];
		const size_t low = i > window ? i - window : 0;
		const size_t high = u_min2sz(i + window + 1, seq_len);

		u_ofoffset(float, aa, off_dst_aa) = iupred2a_mean_inline(ctx, off_src_aa, low, high);
	}
}

static void iupred2a_compute_mode(struct iupred2a_context *ctx, enum iupred_mode_type type)
{
	const struct iupred_loader_context *loader = ctx->loader;
	const struct iupred_mode *minfo = &loader->mode[type];
	const size_t seq_len = ctx->seq_len;
	struct iupred2a_aa *seq = ctx->seq;
	struct aa_freq2_context aa_freq2_ctx;

	/* Compute unweighted score */

	iupred2a_aa_freq2_init(&aa_freq2_ctx, seq_len, seq);
	iupred2a_aa_freq2_set(&aa_freq2_ctx,
			-minfo->params.uc,     minfo->params.uc - minfo->params.lc,
			 minfo->params.lc + 1, minfo->params.uc - minfo->params.lc);

	for (size_t i = 0; i < seq_len; i++)
	{
		struct iupred2a_aa *aa = &seq[i];
		const float *e = minfo->matrix.e[aa->aa];
		const float itotal = 1.f / aa_freq2_ctx.total;
		float score = 0.f;

		for (size_t j = 0; j < U_ARRAY_SIZE(aa_freq2_ctx.aa_count); j++)
		{
			const float freq = aa_freq2_ctx.aa_count[j] * itotal;

			score += e[j] * freq;
		}

		if (aa_freq2_ctx.total <= 0)
			score = 0.f;

		aa->unweighted_score = score;

		iupred2a_aa_freq2_rshift1(&aa_freq2_ctx);
	}

	const size_t range_size = minfo->params.wc;
	const size_t range_bilat_size = range_size * 2 + 1;
	const float ihstep = 1.f / minfo->histo.step;

	/* Compute weighted and adjusted scores */

	for (size_t i = 0; i < seq_len; i++)
	{
		size_t skip_count = 0;
		float score = 0.f;

		for (size_t j = 0; j < range_bilat_size; j++)
		{
			const long idx = (long)i + j - range_size;

			if (idx < 0 || idx >= (long)seq_len)
			{
				score += minfo->params.unweighted_score_adj;
				skip_count++;
				continue;
			}

			score += seq[idx].unweighted_score;
		}

		/* Discard skip count if adjustment was needed */
		if (minfo->params.unweighted_score_adj != 0.f)
			skip_count = 0;

		if (range_bilat_size > skip_count)
			score /= range_bilat_size - skip_count;
		else
			score = 0.f;

		seq[i].weighted_score = score;

		if (score <= minfo->histo.lbound)
			score = 1.f;
		else if (score >= minfo->histo.hbound)
			score = 0.f;
		else
			score = minfo->histo.vals[(size_t)((score - minfo->histo.min) * ihstep)];

		seq[i].mode[type].score = score;
	}

	int in_dom = 0;
	int can_merge = 0;
	int write_dom = 0;
	/* [beg, end] */
	size_t beg = 0;
	size_t end = 0;

	for (size_t i = 0; i < seq_len; i++)
	{
		struct iupred2a_aa *aa = &seq[i];

		seq[i].mode[type].in_dom = 0;

		if (!in_dom)
		{
			if (aa->weighted_score > 0.3f)
			{
				/* New domain */

				if (!can_merge)
					beg = i;

				end = i;
				in_dom = 1;
			}
			else if (i >= end + 45 - 1)
			{
				/* end + 45 is the first aa too far away to be merged */
				/* So end + 45 - 1 is the last aa that can start a new mergeable domain */
				/* Here, end + 45 - 1 is reached and cannot start a domain, so the merge window is closed */
				write_dom = 1;
				can_merge = 0;
			}
		}
		else
		{
			if (aa->weighted_score > 0.3f)
			{
				/* Grow the current domain */
				end = i;
			}
			else
			{
				/* Leave the domain */
				in_dom = 0;
				can_merge = 1;
			}
		}

		if (i == seq_len - 1)
			write_dom = 1;

		if (write_dom)
		{
			if (end - beg + 1 >= 35)
			{
				/* The domain is long enough */
				for (size_t j = beg; j <= end; j++)
					seq[j].mode[type].in_dom = 1;
			}

			write_dom = 0;
		}
	}
}

int iupred2a_init(struct iupred2a_context *ctx, const struct iupred_loader_context *loader)
{
	memset(ctx, 0, sizeof(*ctx));
	ctx->loader = loader;
	return 0;
}

void iupred2a_release(struct iupred2a_context *ctx)
{
	u_free_p(&ctx->seq);
}

void iupred2a_deinit(struct iupred2a_context *ctx)
{
	iupred2a_release(ctx);
	ctx->loader = NULL;
}

static void iupred2a_compute_anchor2(struct iupred2a_context *ctx, enum iupred_mode_type type)
{
	const struct iupred_loader_context *loader = ctx->loader;
	const struct iupred_anchor2 *anchor2 = &loader->anchor2;
	struct iupred2a_aa *seq = ctx->seq;
	struct aa_freq2_context aa_freq2_ctx;
	const float iupred_limit = anchor2->params.c - (anchor2->params.a / anchor2->params.b);

	iupred2a_aa_freq2_init(&aa_freq2_ctx, ctx->seq_len, seq);
	iupred2a_aa_freq2_set(&aa_freq2_ctx,
			-anchor2->params.local_window_size, anchor2->params.local_window_size - 1,
			 2,                                 anchor2->params.local_window_size - 1);

	for (size_t i = 0; i < ctx->seq_len; i++)
	{
		struct iupred2a_aa *aa = &seq[i];
		const float *e = anchor2->matrix.e[aa->aa];
		const float itotal = 1.f / aa_freq2_ctx.total;
		float loc_score = 0.f;
		float iface_score = 0.f;

		for (size_t j = 0; j < U_ARRAY_SIZE(aa_freq2_ctx.aa_count); j++)
		{
			const float loc_freq = aa_freq2_ctx.aa_count[j] * itotal;
			const float iface_freq = anchor2->iface_comp.freq[j];

			if (aa_freq2_ctx.total > 0)
				loc_score += e[j] * loc_freq;
			iface_score += e[j] * iface_freq;
		}

		aa->energy_gain0 = loc_score - iface_score;

		iupred2a_aa_freq2_rshift1(&aa_freq2_ctx);
	}

	iupred2a_smooth(ctx,
			offsetof(struct iupred2a_aa, mode)
				+ type * sizeof(struct iupred2a_aa_mode)
				+ offsetof(struct iupred2a_aa_mode, score),
			offsetof(struct iupred2a_aa, iupred_score),
			anchor2->params.iupred_window_size);

	iupred2a_smooth(ctx,
			offsetof(struct iupred2a_aa, energy_gain0),
			offsetof(struct iupred2a_aa, energy_gain1),
			anchor2->params.local_smoothing_window);
	iupred2a_smooth(ctx,
			offsetof(struct iupred2a_aa, energy_gain1),
			offsetof(struct iupred2a_aa, energy_gain0),
			anchor2->params.local_smoothing_window);

	for (size_t i = 0; i < ctx->seq_len; i++)
	{
		struct iupred2a_aa *aa = &seq[i];
		int sign = 1.f;
		float corr = 0.f;
		float score;

		if (aa->energy_gain0 < anchor2->params.b && aa->iupred_score < anchor2->params.c)
			sign = -1.f;

		if (aa->iupred_score > iupred_limit && aa->energy_gain0 < 0.f)
			corr = (anchor2->params.a / (aa->iupred_score - anchor2->params.c)) + anchor2->params.b;

		score = sign * (aa->energy_gain0 + corr - anchor2->params.b) * (aa->iupred_score - anchor2->params.c);
		score = 1.f / (1.f + expf(-22.97968f * (score - 0.0116f)));

		aa->anchor2_score = score;
	}
}

static int iupred2a_alloc_grow(struct iupred2a_context *ctx, size_t new_len)
{
	size_t new_size = new_len + 1;
	if (new_size <= ctx->alloc_size)
		return 0;

	ctx->seq = realloc(ctx->seq, new_size * sizeof(ctx->seq[0]));
	ctx->alloc_size = new_size;
	return 0;
}

int iupred2a_compute(struct iupred2a_context *ctx, const char *seq)
{
	ctx->seq_len = strlen(seq);
	iupred2a_alloc_grow(ctx, ctx->seq_len);

	for (size_t i = 0; i < ctx->seq_len; i++)
		ctx->seq[i].aa = u_aa_type_from_char(seq[i]);

	iupred2a_compute_mode(ctx, IUPRED_MODE_SHORT);
	iupred2a_compute_mode(ctx, IUPRED_MODE_LONG);
	iupred2a_compute_mode(ctx, IUPRED_MODE_GLOB);
	iupred2a_compute_anchor2(ctx, IUPRED_MODE_LONG);

	return 0;
}

void iupred2a_print(const struct iupred2a_context *ctx, FILE *fp)
{
	for (size_t i = 0; i < ctx->seq_len; i++)
	{
		struct iupred2a_aa *aa = &ctx->seq[i];

		fprintf(fp, "%zu\t%c\t%f\t%c\t%f\t%c\t%f\t%c\t%f\n",
				i, u_aa_type_to_char(aa->aa),
				aa->mode[IUPRED_MODE_SHORT].score, aa->mode[IUPRED_MODE_SHORT].in_dom ? '1' : '0',
				aa->mode[IUPRED_MODE_LONG].score,  aa->mode[IUPRED_MODE_LONG].in_dom ? '1' : '0',
				aa->mode[IUPRED_MODE_GLOB].score,  aa->mode[IUPRED_MODE_GLOB].in_dom ? '1' : '0',
				aa->anchor2_score);
	}
}
