#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util/file.h"
#include "util/types.h"
#include "util/util.h"

#include "loader.h"

enum loader_error
{
	LOADER_ERROR_NONE = 0,
	LOADER_ERROR_EXTRA_FIELD,
	LOADER_ERROR_INCOMPLETE_RECORD,
	LOADER_ERROR_INVALID_FIELD
};

struct parse_state
{
	struct iupred_loader_context *ctx;
	enum loader_error err;
};

static int iupred_loader_process_line_params_cb(struct parse_state *state, char *l, void *param)
{
	struct iupred_mode_params *params = param;
	char *l_save;
	size_t f_idx = 0;

	while (1)
	{
		char *f;

		f = u_strtok_r(l, "\t ", &l_save);
		l = NULL;

		if (!f)
		{
			/* Ignore blank lines */
			if (f_idx == 0)
				return 0;

			if (f_idx <= 3)
			{
				state->err = LOADER_ERROR_INCOMPLETE_RECORD;
				goto end;
			}

			break;
		}

		/* Skip comments */
		if (f_idx == 0 && f[0] == '#')
			return 0;

		switch (f_idx)
		{
			case 0: params->lc = strtoul(f, NULL, 10); break;
			case 1: params->uc = strtoul(f, NULL, 10); break;
			case 2: params->wc = strtoul(f, NULL, 10); break;
			case 3: params->unweighted_score_adj = strtof(f, NULL); break;
			default:
				state->err = LOADER_ERROR_EXTRA_FIELD;
				goto end;
		}

		f_idx++;
	}

end:
	if (state->err != LOADER_ERROR_NONE)
		return -1;

	/* End after the first record */
	return 1;
}

static int iupred_loader_process_line_matrix_cb(struct parse_state *state, char *l, void *param)
{
	struct iupred_energy_matrix *matrix = param;
	char *l_save;
	size_t f_idx = 0;
	int aa0;
	int aa1;
	float score;

	while (1)
	{
		char *f;

		f = u_strtok_r(l, "\t ", &l_save);
		l = NULL;

		if (!f)
		{
			/* Ignore blank lines */
			if (f_idx == 0)
				return 0;

			if (f_idx <= 2)
			{
				state->err = LOADER_ERROR_INCOMPLETE_RECORD;
				goto end;
			}

			break;
		}

		/* Skip comments */
		if (f_idx == 0 && f[0] == '#')
			return 0;

		switch (f_idx)
		{
			case 0: aa0 = u_aa_type_from_char(f[0]); break;
			case 1: aa1 = u_aa_type_from_char(f[0]); break;
			case 2: score = strtof(f, NULL); break;
			default:
				state->err = LOADER_ERROR_EXTRA_FIELD;
				goto end;
		}

		f_idx++;
	}

	if (aa0 == U_AA_INVALID
			|| aa1 == U_AA_INVALID)
	{
		state->err = LOADER_ERROR_INVALID_FIELD;
		goto end;
	}

	matrix->e[aa0][aa1] = score;

end:
	if (state->err != LOADER_ERROR_NONE)
		return -1;
	return 0;
}

static int iupred_loader_process_line_histogram_cb(struct parse_state *state, char *l, void *param)
{
	struct iupred_histogram *histo = param;
	char *l_save;
	size_t f_idx = 0;
	float h;
	float val;

	while (1)
	{
		char *f;

		f = u_strtok_r(l, "\t ", &l_save);
		l = NULL;

		if (!f)
		{
			/* Ignore blank lines */
			if (f_idx == 0)
				return 0;

			if (f_idx <= 4)
			{
				state->err = LOADER_ERROR_INCOMPLETE_RECORD;
				goto end;
			}

			break;
		}

		/* Skip comments */
		if (f_idx == 0 && f[0] == '#')
			return 0;

		switch (f_idx)
		{
			
			case 0: break;
			case 1: h = strtof(f, NULL); break;
			case 2: break;
			case 3: break;
			case 4: val = strtof(f, NULL); break;
			default:
				state->err = LOADER_ERROR_EXTRA_FIELD;
				goto end;
		}

		f_idx++;
	}

	if (h < histo->min)
		histo->min = h;
	if (h > histo->max)
		histo->max = h;

	histo->vals = realloc(histo->vals, (histo->val_count + 1) * sizeof(histo->vals[0]));
	histo->vals[histo->val_count++] = val;

end:
	if (state->err != LOADER_ERROR_NONE)
		return -1;
	return 0;
}

static int iupred_loader_process_line_anchor2_params_cb(struct parse_state *state, char *l, void *param)
{
	struct iupred_anchor2_params *params = param;
	char *l_save;
	size_t f_idx = 0;

	while (1)
	{
		char *f;

		f = u_strtok_r(l, "\t ", &l_save);
		l = NULL;

		if (!f)
		{
			/* Ignore blank lines */
			if (f_idx == 0)
				return 0;

			if (f_idx <= 5)
			{
				state->err = LOADER_ERROR_INCOMPLETE_RECORD;
				goto end;
			}

			break;
		}

		/* Skip comments */
		if (f_idx == 0 && f[0] == '#')
			return 0;

		switch (f_idx)
		{
			case 0: params->local_window_size = strtoul(f, NULL, 10); break;
			case 1: params->iupred_window_size = strtoul(f, NULL, 10); break;
			case 2: params->local_smoothing_window = strtoul(f, NULL, 10); break;
			case 3: params->a = strtof(f, NULL); break;
			case 4: params->b = strtof(f, NULL); break;
			case 5: params->c = strtof(f, NULL); break;
			default:
				state->err = LOADER_ERROR_EXTRA_FIELD;
				goto end;
		}

		f_idx++;
	}

end:
	if (state->err != LOADER_ERROR_NONE)
		return -1;

	/* End after the first record */
	return 1;
}

static int iupred_loader_process_line_anchor2_iface_comp_cb(struct parse_state *state, char *l, void *param)
{
	struct iupred_anchor2_iface_comp *iface_comp = param;
	char *l_save;
	size_t f_idx = 0;
	unsigned char aa;
	float freq;

	while (1)
	{
		char *f;

		f = u_strtok_r(l, "\t ", &l_save);
		l = NULL;

		if (!f)
		{
			/* Ignore blank lines */
			if (f_idx == 0)
				return 0;

			if (f_idx <= 2)
			{
				state->err = LOADER_ERROR_INCOMPLETE_RECORD;
				goto end;
			}

			break;
		}

		/* Skip comments */
		if (f_idx == 0 && f[0] == '#')
			return 0;

		switch (f_idx)
		{
			
			case 0: break;
			case 1: aa = u_aa_type_from_char(f[0]); break;
			case 2: freq = strtof(f, NULL); break;
			default:
				state->err = LOADER_ERROR_EXTRA_FIELD;
				goto end;
		}

		f_idx++;
	}

	iface_comp->freq[aa] = freq;

end:
	if (state->err != LOADER_ERROR_NONE)
		return -1;
	return 0;
}

typedef int (*loader_line_cb_t)(struct parse_state *state, char *line, void *param);

static int iupred_loader_process_file(struct iupred_loader_context *ctx, const char *filename, loader_line_cb_t cb, void *cb_param)
{
	struct parse_state state;
	int result;
	struct u_file file;
	char *data;
	char *d;
	char *d_save;

	state.ctx = ctx;
	state.err = LOADER_ERROR_NONE;

	result = u_file_open(&file, filename, U_FILE_OPEN_READ);
	if (result < 0)
		return -1;

	data = u_file_get_contents(&file, NULL);
	d = data;

	u_file_close(&file);

	while (1)
	{
		char *l;

		l = u_strtok_r(d, "\r\n", &d_save);
		d = NULL;

		if (!l)
			break;

		result = cb(&state, l, cb_param);
		if (result != 0)
			goto end;
	}

end:
	free(data);

	if (state.err != LOADER_ERROR_NONE)
		return -1;
	return 0;
}

static int iupred_loader_process_mode(struct iupred_loader_context *ctx, const char *path, enum iupred_mode_type type)
{
	static const char *filename[IUPRED_MODE_COUNT] = {
		[IUPRED_MODE_SHORT] = "short",
		[IUPRED_MODE_LONG] = "long",
		[IUPRED_MODE_GLOB] = "glob"
	};
	const char *fname = filename[type];
	char *fullpath;
	struct iupred_mode *mode = &ctx->mode[type];

	memset(&mode->params, 0, sizeof(mode->params));
	memset(&mode->matrix, 0, sizeof(mode->matrix));
	memset(&mode->histo, 0, sizeof(mode->histo));

	fullpath = u_str_concat(path, "/", fname, "_params", NULL);
	iupred_loader_process_file(ctx, fullpath, iupred_loader_process_line_params_cb, &mode->params);
	free(fullpath);

	fullpath = u_str_concat(path, "/iupred2_", fname, "_energy_matrix", NULL);
	iupred_loader_process_file(ctx, fullpath, iupred_loader_process_line_matrix_cb, &mode->matrix);
	free(fullpath);

	fullpath = u_str_concat(path, "/", fname, "_histogram", NULL);
	iupred_loader_process_file(ctx, fullpath, iupred_loader_process_line_histogram_cb, &mode->histo);
	free(fullpath);

	mode->histo.step = (mode->histo.max - mode->histo.min) / mode->histo.val_count;
	mode->histo.lbound = mode->histo.min + 2.f * mode->histo.step;
	mode->histo.hbound = mode->histo.max - 2.f * mode->histo.step;

	return 0;
}

static int iupred_loader_process_anchor2(struct iupred_loader_context *ctx, const char *path)
{
	char *fullpath;
	struct iupred_anchor2 *anchor2 = &ctx->anchor2;

	memset(&anchor2->matrix, 0, sizeof(anchor2->matrix));
	memset(&anchor2->iface_comp, 0, sizeof(anchor2->iface_comp));

	fullpath = u_str_concat(path, "/anchor2_params", NULL);
	iupred_loader_process_file(ctx, fullpath, iupred_loader_process_line_anchor2_params_cb, &anchor2->params);
	free(fullpath);

	fullpath = u_str_concat(path, "/anchor2_energy_matrix", NULL);
	iupred_loader_process_file(ctx, fullpath, iupred_loader_process_line_matrix_cb, &anchor2->matrix);
	free(fullpath);

	fullpath = u_str_concat(path, "/anchor2_interface_comp", NULL);
	iupred_loader_process_file(ctx, fullpath, iupred_loader_process_line_anchor2_iface_comp_cb, &anchor2->iface_comp);
	free(fullpath);

	return 0;
}

int iupred_loader_init(struct iupred_loader_context *ctx, const char *path)
{
	ctx->path = path;
	return 0;
}

int iupred_loader_deinit(struct iupred_loader_context *ctx)
{
	(void)ctx;
	return 0;
}

int iupred_loader_process(struct iupred_loader_context *ctx)
{
	iupred_loader_process_mode(ctx, ctx->path, IUPRED_MODE_SHORT);
	iupred_loader_process_mode(ctx, ctx->path, IUPRED_MODE_LONG);
	iupred_loader_process_mode(ctx, ctx->path, IUPRED_MODE_GLOB);
	iupred_loader_process_anchor2(ctx, ctx->path);
	return 0;
}
