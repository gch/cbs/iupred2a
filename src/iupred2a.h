#ifndef IUPRED2A_H
#define IUPRED2A_H 1

#include <stdio.h>

#include "loader.h"

#include "util/aa.h"
#include "util/types.h"

struct iupred2a_aa_mode
{
	unsigned char in_dom;
	float score;
};

struct iupred2a_aa
{
	enum u_aa_type aa;
	struct iupred2a_aa_mode mode[IUPRED_MODE_COUNT];
	float anchor2_score;

	/* Cache */
	union
	{
		struct
		{
			float unweighted_score;
			float weighted_score;
		};

		struct
		{
			float iupred_score;
			float energy_gain0;
			float energy_gain1;
		};
	};
};

struct iupred2a_context
{
	const struct iupred_loader_context *loader;
	size_t alloc_size;
	size_t seq_len;
	struct iupred2a_aa *seq;
};

extern float iupred2a_sum(struct iupred2a_context *ctx, size_t off_prop, int32_t low, int32_t high);
extern float iupred2a_mean(struct iupred2a_context *ctx, size_t off_prop, int32_t low, int32_t high);
extern int iupred2a_init(struct iupred2a_context *ctx, const struct iupred_loader_context *loader);
extern void iupred2a_release(struct iupred2a_context *ctx);
extern void iupred2a_deinit(struct iupred2a_context *ctx);
extern int iupred2a_compute(struct iupred2a_context *ctx, const char *seq);
extern void iupred2a_print(const struct iupred2a_context *ctx, FILE *fp);

#endif /* IUPRED2A_H */
